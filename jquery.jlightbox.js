/*
* $jlightbox
* Author: Josh White
* Version : 1.0

* Options:
*	overlayClass 		- defines class applied to opaque overlay
*	closeButtonClass	- defines class which will toggle lightbox close
*	offsetTop			- offset from top of window
*	fadeDuration		- duration of jQuery fade effect

* Examples:
	Default: $('#lightbox').jlightbox();
	Bespoke: $('#lightbox').jlightbox({
								overlayClass: "overlay", 
								closeButtonClass: "closeoverlay", 
								offsetTop: "100px", 
								fadeDuration: "2000",
								forceWidth: "400"
							});

*/
(function($){ 
	jQuery.fn.extend({

		jlightbox: function(options) {
			
			// Default options
			var defaults = {  
				overlayClass: 		"jlightbox_overlay",
				closeButtonClass: 	"close",
				offsetTop: 			"40px",
				fadeDuration: 		"500"
			};
			// Create options 
			var options = $.extend(defaults, options);  
			
			// To return each time plugin is called
			return this.each(function() {   
			
				var	$this = 	$(this),
					$width = 	options.forceWidth || $this.width(),
					$offset = 	$width / 2,
					$overlay = 	"<div class='" + options.overlayClass + "'></div>";
					$close = 	$this.find("." + options.closeButtonClass);

				// Positions modal
				$this
					.css({
						'background': 		'#fff',
						'left' :			'50%',
						'display': 			'none',
						'margin-left':		'-' + $offset + 'px',
						'z-index': 			'9999',
						'position': 		'fixed',
						'top': 				options.offsetTop,
						'display': 			'block',
						'width': 			$width + 'px'
					})
					.addClass('jlightboxed')

				// Appends overlay
				$('body').append($overlay);
				$jlightbox_overlay = $("." + options.overlayClass);

				$jlightbox_overlay.css({
					'background': 	'rgb(0, 0, 0)',
					'left': 		0,
					'display': 		'none',
					'height': 		'100%',
					'top': 			0,
					'opacity': 		.3,
					'position' : 	'fixed',
					'width' : 		'100%',
					'z-index' : 	1000
				});

				// Sets up collections
				var $fadeElements =		 $jlightbox_overlay.add($this);
				var $clickableElements = $jlightbox_overlay.add($close);

				// Fade in
				$fadeElements.fadeIn(options.fadeDuration);

				// Fade Out
				$clickableElements.on('click', function(e) {
					e.preventDefault();

					$fadeElements.fadeOut(options.fadeDuration);
				});

				// Add 'options.' before the value 
				jQuery(this).css(options.property, options.value);
			});
		}
	});
})(jQuery); 